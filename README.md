# VueJS Application with Vuetify #

This Application working with REST API. We can create clients and providers and after that connect the provider(s) to clients.

### How run Client Providers VueJS Application ? ###

* Clone repository - [git clone https://aren_amirjanyan@bitbucket.org/aren_amirjanyan/client-provider-app.git](git clone https://aren_amirjanyan@bitbucket.org/aren_amirjanyan/client-provider-app.git)
* npm install
* check /src/main.js and if you need change Vue.config.API (API URL) credential
* npm run serve
