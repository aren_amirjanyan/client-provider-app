import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import VueLocalStorage from 'vue-localstorage'
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);

Vue.config.productionTip = false;

Vue.config.API = 'http://localhost:3000/api/v1';

import tokenService from './services/TokenService'

// index.js or main.js
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader

Vue.use(VueLocalStorage);

    tokenService.getToken().then(response => {
        Vue.localStorage.set('Authorization', response);
    }).catch(error=>{console.log(error)});

new Vue({
  render: h => h(App)
}).$mount('#app');
