import axios from 'axios';
import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'

Vue.use(VueLocalStorage);


class ProviderService {

    constructor(){
        this.options =  {
            headers : {
                "Authorization" : `Bearer ${Vue.localStorage.get('Authorization')}`
            }
        }
    }
    /**
     *
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    getAllProviders() {
        return axios.get(`${Vue.config.API}/providers`,this.options).then(response => {
            if(response.data.success){
                return response.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

    /**
     *
     * @param providerId
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    getProviderById(providerId){
        return axios.get(`${Vue.config.API}/providers/${providerId}`,this.options).then(response => {
            if(response.data.success && response.data.data.length > 0){
                return response.data.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

    /**
     *
     * @param request
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    create(request){
        return axios.post(`${Vue.config.API}/providers`,request,this.options).then(response => {
            if(response.data.success && response.data.data){
                return response.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

    /**
     *
     * @param providerId
     * @param request
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    update(providerId,request){
        return axios.put(`${Vue.config.API}/providers/${providerId}`,request,this.options).then(response => {
            if(response.data.success && response.data.data){
                return response.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

    /**
     *
     * @param providerId
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    delete(providerId){
        return axios.delete(`${Vue.config.API}/providers/${providerId}`,this.options).then(response => {
            if(response.data.success){
                return response.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

}
export default new ProviderService();