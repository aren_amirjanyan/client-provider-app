import axios from 'axios';
import Vue from 'vue'

class TokenService {

    getToken() {
        return axios.get(`${Vue.config.API}/getToken`).then(response => {
            if(response.data.success && response.data.token ){
                return response.data.token;
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }
}
export default new TokenService();