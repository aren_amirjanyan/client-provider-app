import axios from 'axios';
import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'

Vue.use(VueLocalStorage);

class ClientService {

    constructor(){
        this.options =  {
            headers : {
                "Authorization" : `Bearer ${Vue.localStorage.get('Authorization')}`
            }
        }
    }
    /**
     *
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    getAllClients() {
        return axios.get(`${Vue.config.API}/clients`,this.options).then(response => {
            if(response.data.success){
                return response.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

    /**
     *
     * @param clientId
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    getClientById(clientId){
        return axios.get(`${Vue.config.API}/clients/${clientId}`,this.options).then(response => {
            if(response.data.success && response.data.data.length > 0){
                return response.data.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

    /**
     *
     * @param request
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    create(request){
        return axios.post(`${Vue.config.API}/clients`,request,this.options).then(response => {
            if(response.data.success && response.data.data){
                return response.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

    /**
     *
     * @param clientId
     * @param request
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    update(clientId,request){
        return axios.put(`${Vue.config.API}/clients/${clientId}`,request,this.options).then(response => {
            if(response.data.success && response.data.data){
                return response.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

    /**
     *
     * @param clientId
     * @returns {Promise<AxiosResponse<any> | {data: Array, success: boolean, error: boolean, message: *}>}
     */
    delete(clientId){
        return axios.delete(`${Vue.config.API}/clients/${clientId}`,this.options).then(response => {
            if(response.data.success){
                return response.data;
            }else{
                return {data : [], success : false, error : true, message : response.data.message}
            }
        }).catch(error=>{return {data : [], success : false, error : true, message : error.message}});
    }

}
export default new ClientService();